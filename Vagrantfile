# -*- mode: ruby -*-
# -*- coding: utf-8 -*-

require 'fileutils'

hosts = {
    "chost1" => "192.168.168.10",
    "chost2" => "192.168.168.20",
}

folders = {
    "./build" => "/home/vagrant/build",
    "./app" => "/home/vagrant/app",
}

Vagrant.configure(2) do |config|
    # constants
    VAGRANT_HOSTS = 2
    VAGRANT_VM_PROVIDER   = "virtualbox"
    VAGRANT_ROOT = File.dirname(__FILE__)

    ANSIBLE_INVENTORY_FILE = "#{VAGRANT_ROOT}/ansible/hosts"
    ANSIBLE_SETUP_PLAYBOOK = "#{VAGRANT_ROOT}/ansible/setup.yml"

    # recreate inventory
    FileUtils.rm ANSIBLE_INVENTORY_FILE if File.exists?(ANSIBLE_INVENTORY_FILE)
    File.open("#{ANSIBLE_INVENTORY_FILE}" ,'w') do | f |
        f.write "[all]\n"
        hosts.each do |name, ip|
            f.write "#{ip} ansible_user=vagrant ansible_ssh_private_key_file=#{VAGRANT_ROOT}/.vagrant/machines/#{name}/#{VAGRANT_VM_PROVIDER}/private_key\n"
        end
        f.write "\n"
    end

    # ---
    config.vm.box = "debian/buster64"
    config.vm.box_check_update = false

    config.vm.synced_folder ".", "/vagrant", disabled: true
    folders.each do |src, dest|
        config.vm.synced_folder src, dest,
            owner: "vagrant",
            group: "vagrant",
            type: "rsync",
            rsync__auto: true
    end

    hosts.each do |name, ip|
        File.open("#{ANSIBLE_INVENTORY_FILE}" ,'a') do | f |
            f.write "[#{name}]\n"
            f.write "#{ip}\n"
            f.write "\n"
        end

        config.vm.define "#{name}" do |machine|
            machine.vm.hostname = name
            machine.vm.network :private_network, ip: ip
            machine.vm.provider "#{VAGRANT_VM_PROVIDER}" do |vb|
                vb.name = name
                vb.gui = false
                vb.cpus = 2
                vb.memory = "512"
                vb.functional_vboxsf = false
                vb.check_guest_additions = false
            end
        end
    end

    config.vm.provision "ansible" do |ansible|
        ansible.compatibility_mode = "2.0"
        ansible.verbose = false
        ansible.host_key_checking = false
        ansible.playbook = "#{ANSIBLE_SETUP_PLAYBOOK}"
        ansible.extra_vars = { ansible_python_interpreter:"/usr/bin/python" }
    end

end
