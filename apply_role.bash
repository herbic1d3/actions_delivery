#!/usr/bin/env bash

IP_HOST=$(eval "awk -F \"\\\"\" '/$1/ {print \$4}' Vagrantfile")

ansible-playbook  \
    -e 'ansible_python_interpreter=/usr/bin/python' \
    --connection=ssh \
    --timeout=30 \
    --inventory-file=./ansible/hosts \
    --extra-vars "target=$IP_HOST" \
    -t $2 \
    ./ansible/apply_role.yml \
    2>&1
