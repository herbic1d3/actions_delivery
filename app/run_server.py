import os
import random
import datetime

os.environ['DJANGO_SETTINGS_MODULE'] = 'config.settings'

import django
from django.conf import settings
from django.contrib.auth.management.commands.createsuperuser import get_user_model

django.setup()

# --------------------
# create superuser if not exist

User = get_user_model()
if User.objects.filter(email=os.environ.get('DJANGO_SU_EMAIL')).exists():
    print('Super user already exists. SKIPPING...')
else:
    print('Creating super user...')
    User.objects.create_superuser(
        name=os.environ.get('DJANGO_SU_EMAIL').split('@')[0],
        email=os.environ.get('DJANGO_SU_EMAIL'),
        password=os.environ.get('DJANGO_SU_PASSWORD'),
    )
    print('Super user created...')

# --------------------
# create clickhouse database and table
from clickhouse_driver import Client

client = Client(host=settings.CLICKHOUSE_DATABASE['host'])
client.execute("CREATE DATABASE IF NOT EXISTS {}".format(settings.CLICKHOUSE_DATABASE['database']))

from config.clickhouse import clickhouse_db
clickhouse_db.create_all()

# --------------------
# fill initial data

for item in range(5):
    email = 'usual{}@u.com'.format(item)
    if not User.objects.filter(email=email).exists():
        User.objects.create_user(
            name=email.split('@')[0],
            email=email,
            date_joined=(datetime.date.today() - datetime.timedelta(days=random.randint(4, 8))),
            date_registration=(datetime.date.today() - datetime.timedelta(days=random.randint(1, 3))),
            password='123456',
            is_guest=bool(random.getrandbits(1))
        )


