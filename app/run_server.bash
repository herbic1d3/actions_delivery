#!/bin/bash

WAITED_HOST="psql"
WAITED_PORT="5432"

if [ "$DJANGO_SETTINGS_MODULE_MODE" != "dev" ]; then WAITED_HOST="dbserver"; fi

cd /app/

# waiting up postgresql server
while [ $(timeout 2 bash -c "</dev/tcp/$WAITED_HOST/$WAITED_PORT" >/dev/null 2>&1 ; echo $?) != 0 ]; do
    echo "Wait `$WAITED_HOST:$WAITED_PORT` not started, waiting..."
done

if [[ ! -e /migrated.tmp ]]; then
    echo "Migrating the database before starting the server"
    python manage.py makemigrations
    python manage.py migrate
    echo yes | python manage.py collectstatic

    if [ "$DJANGO_SETTINGS_MODULE_MODE" != "dev" ]; then
        cp -R /app/static/admin /app/www/
    fi

    if [[ $? -eq  0 ]]; then
        touch /migrated.tmp
    else
        exit
    fi
fi


# update database configuration
python run_server.py


# Start processes
gunicorn config.wsgi:application --reload --workers 3 --bind 0.0.0.0:8000
