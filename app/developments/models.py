from django.db import models
from django.utils import timezone
from django.db.models.signals import post_save
from django.dispatch import receiver

from core.constants import DEVELOPMENT_ACTION_CHOICES, DEVELOPMENT_VIEW
from objectives.models import Objective


class Development(models.Model):
    """
        development status for user objectives
    """
    time = models.DateTimeField(default=timezone.now)
    action_id = models.IntegerField(choices=DEVELOPMENT_ACTION_CHOICES, default=DEVELOPMENT_VIEW)
    target = models.OneToOneField(Objective, on_delete=models.CASCADE)


@receiver(post_save, sender=Development, dispatch_uid="update_sync_queue")
def update_sync_queue(sender, instance, **kwargs):
    from syncdata.models import SyncObject

    if not SyncObject.objects.filter(development_id=instance.id).exists():
        SyncObject.objects.create(development_id=instance.id)
