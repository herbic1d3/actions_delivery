from django.test import TestCase
from django.contrib.auth import get_user_model

from core.constants import DEVELOPMENT_VIEW, DEVELOPMENT_SUBMIT, DEVELOPMENT_RESOLVE
from objectives.models import Objective
from developments.models import Development


class TestDevelopment(TestCase):
    
    def setUp(self):
        User = get_user_model()
        self.user1 = User.objects.create_user(email='usual1@u.com', password='qwerty')
        self.user2 = User.objects.create_user(email='usual2@u.com', password='qwerty')

        self.objective_11 = Objective.objects.create(user=self.user1)
        self.objective_12 = Objective.objects.create(user=self.user1)
        
        self.objective_21 = Objective.objects.create(user=self.user2)

    def test_create_items(self):
        Development.objects.create(target=self.objective_11)
        Development.objects.create(target=self.objective_12)
        dev = Development.objects.create(target=self.objective_21)

        self.assertEqual(Development.objects.all().count(), 3)
        self.assertEqual(Development.objects.filter(action_id=DEVELOPMENT_SUBMIT).count(), 0)

        dev.action_id = DEVELOPMENT_SUBMIT
        dev.save()
        self.assertEqual(Development.objects.filter(action_id=DEVELOPMENT_SUBMIT).count(), 1)
        self.assertEqual(Development.objects.filter(action_id=DEVELOPMENT_RESOLVE).count(), 0)

        dev.action_id = DEVELOPMENT_RESOLVE
        dev.save()
        self.assertEqual(Development.objects.filter(action_id=DEVELOPMENT_VIEW).count(), 2)
        self.assertEqual(Development.objects.filter(action_id=DEVELOPMENT_SUBMIT).count(), 0)
        self.assertEqual(Development.objects.filter(action_id=DEVELOPMENT_RESOLVE).count(), 1)
        
        self.assertEqual(Development.objects.filter(target__user=self.user2).count(), 1)
        
                