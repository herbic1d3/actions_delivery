from django.conf import settings

from sqlalchemy import create_engine, MetaData
from clickhouse_sqlalchemy import make_session, get_declarative_base


class dbClass(object):
    """
        ClickHouse
    """
    def __init__(self):
        self.engine = create_engine(settings.CLICKHOUSE_URI)
        self.session =make_session(self.engine)
        self.metadata = MetaData(bind=self.engine)
        Base = get_declarative_base(metadata=self.metadata)
        self.Model = Base

    def create_all(self):
        self.metadata.create_all(bind=self.engine)

    def drop_all(self):
        self.metadata.drop_all(bind=self.engine)


clickhouse_db = dbClass()
