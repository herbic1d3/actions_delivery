from config.celery import app
from .systems import insert_objective_record


@app.task(name='generate_new_record')
def generate_new_record():
    """
    schedule an insert new random record
    """
    insert_objective_record()


