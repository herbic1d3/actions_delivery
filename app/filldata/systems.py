import random

from django.contrib.auth import get_user_model

from objectives.models import Objective
from developments.models import Development


def insert_objective_record():
    """
    an insert new random record
    """
    name = 'usual{}'.format(random.randint(0, 4))
    user = get_user_model().objects.get(name=name)
    objective = Objective.objects.create(
        user=user
    )
    Development.objects.create(
        target=objective,
        action_id=random.randint(0, 2)
    )
