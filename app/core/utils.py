
def boolean_to(value):
    """
    convert boolean value to clickhouse int
    """
    return 1 if value else 0

def string_to(value):
    return str('' if value is None else value)
