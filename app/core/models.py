from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.utils.translation import ugettext_lazy as _

from .managers import CustomUserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField(_('email address'), unique=True, blank=False,
        error_messages = {
            'unique': _("A user with that email already exists."),
        },
    )
    name = models.CharField(_('username'), max_length=50, unique=False)
    is_staff = models.BooleanField(default=False)
    is_guest = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    date_registration = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()
   
    def __str__(self):
        return "{}; is_guest: {}".format(self.email, self.is_guest)
