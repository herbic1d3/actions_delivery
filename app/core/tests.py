from django.test import TestCase
from django.contrib.auth import get_user_model


class TestCustomUserManagers(TestCase):

    def setUp(self):
        self.user_object = get_user_model()

    def test_create_user(self):
        u_user = self.user_object.objects.create_user(email='usual@u.com', password='qwerty')
        self.assertFalse(u_user.is_superuser)
        self.assertTrue(u_user.is_guest)

        self.assertIsNotNone(u_user.date_joined)

        with self.assertRaises(TypeError):
            self.user_object.objects.create_user()
        with self.assertRaises(TypeError):
            self.user_object.objects.create_user(email='')
        with self.assertRaises(ValueError):
            self.user_object.objects.create_user(email='', password='qwerty')

    def test_create_superuser(self):
        s_user = self.user_object.objects.create_superuser('super@u.com', 'qwerty')
        self.assertEqual(s_user.email, 'super@u.com')
        self.assertFalse(s_user.is_guest)
        self.assertTrue(s_user.is_superuser)
        self.assertTrue(self.client.login(username='super@u.com', password='qwerty'))

