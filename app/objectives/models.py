from django.db import models
from core.models import CustomUser


class Objective(models.Model):
    """
        objectives by users
    """
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, null=True)
