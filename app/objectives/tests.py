from django.test import TestCase
from django.contrib.auth import get_user_model

from objectives.models import Objective


class TestObjectives(TestCase):
    
    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create_user(email='usual@u.com', password='qwerty')

    def test_create_task(self):
        objective = Objective(id=None)

        self.assertIsNone(objective.id)
        self.assertEqual(Objective.objects.all().count(), 0)

        objective.save()
        self.assertIsNotNone(objective.id)
        self.assertEqual(Objective.objects.all().count(), 1)
        self.assertIsNone(objective.user)
        
        objective.user = self.user
        objective.save()
        self.assertIsNotNone(objective.user)
