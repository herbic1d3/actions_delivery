from django.apps import AppConfig


class SyncdataConfig(AppConfig):
    name = 'syncdata'
