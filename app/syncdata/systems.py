from django.db import transaction

from config.clickhouse import clickhouse_db
from core.utils import boolean_to, string_to

from syncdata.models import SyncObject, Action
from developments.models import Development


@transaction.atomic
def sync_data():
    """
    data synchronization
    """
    ids, data = prepare_upload_data()
    upload_data(data)
    clear_uploaded(ids)


def prepare_upload_data():
    """
    get synchronization objects data
    """
    ids = SyncObject.objects.select_for_update().values_list('development_id', flat=True).order_by('development_id')
    data = Development.objects.filter(pk__in=ids).select_related("target", 'target__user')
    return ids, data


def upload_data(data):
    """
    upload data if exist
    """
    if not data:
        return

    send_data = []
    for item in data:
        send_data.append({
            'day': item.time,
            'time': item.time,
            'user_id': item.target.user_id,
            'date_joined': item.target.user.date_joined,
            'date_registration': item.target.user.date_registration,
            'name': string_to(item.target.user.name),
            'email': string_to(item.target.user.email),
            'is_guest': boolean_to(item.target.user.is_guest),
            'step_id': item.target_id,
            'action_id': item.action_id,
        })
    table = Action.__table__
    return clickhouse_db.session.execute(table.insert(), send_data)


def clear_uploaded(ids):
    """
    clear uploaded data into `SyncObject` models
    """
    SyncObject.objects.filter(development_id__in=ids).delete()
    return True

