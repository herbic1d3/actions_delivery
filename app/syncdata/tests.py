from mock.mock import patch

from django.test import TestCase
from django.contrib.auth import get_user_model

from core.constants import DEVELOPMENT_VIEW, DEVELOPMENT_SUBMIT, DEVELOPMENT_RESOLVE
from objectives.models import Objective
from developments.models import Development
from syncdata.models import SyncObject
from syncdata.tasks import sync_data


class TestSyncObject(TestCase):

    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create_user(email='usual1@u.com', password='qwerty')

        self.objective1 = Objective.objects.create(user=self.user)
        self.objective2 = Objective.objects.create(user=self.user)
        self.objective3 = Objective.objects.create(user=self.user)

    def test_fill_synced_data_on_create(self):
        Development.objects.create(target=self.objective1)
        Development.objects.create(target=self.objective2)
        dev = Development.objects.create(target=self.objective3)

        self.assertEqual(SyncObject.objects.all().count(), 3)

    def test_fill_synced_data_on_action_update(self):
        Development.objects.create(target=self.objective1)
        dev = Development.objects.create(target=self.objective2)

        self.assertEqual(dev.action_id, DEVELOPMENT_VIEW)
        self.assertEqual(SyncObject.objects.all().count(), 2)

        SyncObject.objects.filter(development_id=dev.id).delete()

        self.assertEqual(SyncObject.objects.all().count(), 1)

        dev.action_id = DEVELOPMENT_SUBMIT
        dev.save()
        self.assertEqual(SyncObject.objects.all().count(), 2)

    @patch('syncdata.systems.upload_data', lambda x: True)
    def test_sync_data(self):
        Development.objects.create(target=self.objective1)
        Development.objects.create(target=self.objective2)

        sync_data()

        self.assertEqual(SyncObject.objects.all().count(), 0)
