from django.db import models
from config.clickhouse import clickhouse_db

from sqlalchemy import Column
from sqlalchemy.util import to_list
from clickhouse_sqlalchemy import types, engines

from developments.models import Development


class SyncObject(models.Model):
    """
    store `id` changed development objects
    """
    development = models.OneToOneField(Development, on_delete=models.CASCADE)


class Action(clickhouse_db.Model):
    """
    clickhouse table
    """
    __tablename__ = 'actions'

    day = Column(types.Date, primary_key=True)
    time = Column(types.DateTime)
    user_id = Column(types.UInt32)
    date_joined = Column(types.DateTime)
    date_registration = Column(types.DateTime)
    name = Column(types.String)
    email = Column(types.String)
    is_guest = Column(types.UInt8)
    step_id = Column(types.UInt32)
    action_id = Column(types.UInt8)

    __table_args__ = (
        engines.MergeTree(
            # partition_by=to_list(day, user_id),
            order_by=(day, user_id, step_id, action_id)
        ),
    )

