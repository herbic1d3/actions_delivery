from config.celery import app
from .systems import sync_data


@app.task(name='periodically_sync')
def periodically_sync():
    """
    celery periodic data synchronization
    """
    sync_data()


