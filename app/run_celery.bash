#!/bin/bash

WAITED_HOST="webserver"
WAITED_PORT="8000"

cd /app/

# waiting up django project
while [ $(timeout 2 bash -c "</dev/tcp/$WAITED_HOST/$WAITED_PORT" >/dev/null 2>&1 ; echo $?) != 0 ]; do
    echo "Wait `$WAITED_HOST:$WAITED_PORT` not started, waiting..."
done

sleep 5

# Start processes
celery -A config worker -B -l info
